package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"gitlab.com/shamansanchez/sc4go/dbpf"
)

// CityEntry is a single index entry
type CityEntry struct {
	Name      string
	Mayor     string
	TileX     uint32
	TileY     uint32
	Modified  uint32
	Path      string
	SHA256Sum string
	Mode      uint8
	Owner     string
}

type OwnerRecords []struct {
	UUID string
	Name string
}

func main() {
	log.Println("SC4 Server")
	http.HandleFunc("/city", cityHandler)
	http.HandleFunc("/index", indexHandler)
	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static", fs))
	log.Fatal(http.ListenAndServe("[::1]:9099", nil))
}

func cityHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[%s] [%s %s]", r.Header.Get("X-Forwarded-For"), r.Method, r.URL.String())

	if r.Method == "POST" {
		body, err := ioutil.ReadAll(r.Body)

		if err != nil {
			http.Error(w, err.Error(), 500)
			return
		}

		city, err := dbpf.ReadRawDBPF(body)
		if err != nil {
			http.Error(w, "couldn't read city", 500)
			return
		}

		info := dbpf.GetRegionData(city)

		clientID := r.Header.Get("X-Client-ID")
		coords := fmt.Sprintf("%d.%d", info.TileX, info.TileY)

		log.Printf("[%s - %s]", info.Name, info.MayorName)

		// owner file doesn't exist, must be a new city
		if _, err := os.Stat(coords + ".owner"); os.IsNotExist(err) {
			log.Printf("%s creating %s (%s %X)", clientID, coords, info.Name, city.SHA256Sum)

			// Only claim cities in Mayor mode
			if info.God == 1 {
				ownerFile, _ := os.Create(coords + ".owner")
				defer ownerFile.Close()
				ownerFile.Write([]byte(clientID))
			}

			cityFile, _ := os.Create(coords + ".sc4")
			defer cityFile.Close()
			cityFile.Write(body)

			w.Write([]byte("OK"))
			return
		}

		owner, err := ioutil.ReadFile(coords + ".owner")
		if err != nil {
			http.Error(w, "Couldn't read owner file", 500)
			return
		}

		if string(owner) == clientID {
			log.Println("Owner matches, updating city...")
			cityFile, _ := os.Create(coords + ".sc4")
			defer cityFile.Close()
			cityFile.Write(body)

			// If this city was obliterated, remove the owner
			if info.God == 0 {
				os.Remove(coords + ".owner")
			}
		} else {
			log.Println("Owner mismatch, refusing update!")
		}

	}

	if r.Method == "GET" {
		query, ok := r.URL.Query()["coords"]

		if !ok || len(query[0]) < 1 {
			http.Error(w, "No coordinates provided", 500)
			return
		}

		coords := query[0]

		city, err := dbpf.ReadDBPF(coords + ".sc4")
		if err != nil {
			return
		}
		info := dbpf.GetRegionData(city)

		log.Println(info.Name)
		cityFile, _ := os.Open(coords + ".sc4")
		defer cityFile.Close()

		data, _ := ioutil.ReadAll(cityFile)

		w.Write([]byte(data))

	}

}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	log.Printf("[%s] [%s %s]", r.Header.Get("X-Forwarded-For"), r.Method, r.URL.String())
	paths, err := ioutil.ReadDir(".")

	if err != nil {
		return
	}

	ownerJSON, err := ioutil.ReadFile("owners.json")

	if err != nil {
		http.Error(w, "No owners JSON!", 500)
		return
	}

	owners := OwnerRecords{}
	json.Unmarshal(ownerJSON, &owners)
	entries := make([]CityEntry, 0)

	for _, c := range paths {
		name := c.Name()

		if filepath.Ext(name) == ".sc4" {

			city, err := dbpf.ReadDBPF(name)
			if err != nil {
				return
			}
			info := dbpf.GetRegionData(city)

			coords := fmt.Sprintf("%d.%d", info.TileX, info.TileY)

			var owner string

			if _, err := os.Stat(coords + ".owner"); os.IsNotExist(err) {
				owner = "no one"
			} else {
				ownerBytes, _ := ioutil.ReadFile(fmt.Sprintf("%s.owner", coords))
				ownerUUID := string(ownerBytes)

				for _, o := range owners {
					if o.UUID == ownerUUID {
						owner = o.Name
						break
					}
					owner = "someone? I guess?"
				}
			}

			entry := CityEntry{
				Name:      info.Name,
				Mayor:     info.MayorName,
				TileX:     info.TileX,
				TileY:     info.TileY,
				Modified:  city.Header.Modified,
				Path:      fmt.Sprintf("/city?coords=%s", coords),
				SHA256Sum: fmt.Sprintf("%x", city.SHA256Sum),
				Mode:      info.God,
				Owner:     owner,
			}

			entries = append(entries, entry)

		}
	}
	data, _ := json.Marshal(entries)
	w.Header().Set("Content-Type", "application/json")
	w.Write(data)
}
